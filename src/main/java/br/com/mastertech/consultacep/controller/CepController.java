package br.com.mastertech.consultacep.controller;

import br.com.mastertech.consultacep.clients.BuscaCEP;
import br.com.mastertech.consultacep.model.Cep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cep")
public class CepController {

    @Autowired
    private BuscaCEP buscaCEP;

    @GetMapping("/{cep}")
    @ResponseStatus(HttpStatus.OK)
    public Cep consultaCEP (@PathVariable String cep) {
        return buscaCEP.buscaCEP(cep);
    }
}
