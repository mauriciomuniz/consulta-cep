package br.com.mastertech.consultacep.clients;


import br.com.mastertech.consultacep.model.Cep;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "viacep", url = "https://viacep.com.br/ws/")
public interface BuscaCEP {

    @GetMapping("/{cep}/json/")
    @NewSpan(name = "viacep-client")
    Cep buscaCEP(@PathVariable(name = "cep") String cep);
}

